# -*- coding: utf-8 -*-
from shapely.geometry import Point

import pytest

from pyreproj import InvalidTargetError
from pyreproj import Reprojector


@pytest.mark.parametrize("type,target,from_srs,to_srs", [
    ("tuple", (45.0, 45.0), 'EPSG:4326', 'EPSG:3857'),
    ("list", [45.0, 45.0], 'EPSG:4326', 'EPSG:3857'),
    ("shapely", Point(45.0, 45.0), 'EPSG:4326', 'EPSG:3857'),
    ("invalid", 'not a valid target', 'EPSG:4326', 'EPSG:3857')
])
def test_transform(type, target, from_srs, to_srs):
    rp = Reprojector()
    if type == 'invalid':
        try:
            rp.transform(target, from_srs=from_srs, to_srs=to_srs)
        except InvalidTargetError as e:
            assert e.message == 'Invalid target to transform. Valid targets are [x, y], (x, y) or a shapely' \
                                ' geometry object.'
    else:
        trf = rp.transform(target, from_srs=from_srs, to_srs=to_srs)
        if type == 'tuple':
            assert isinstance(trf, tuple)
            assert len(trf) == 2
        elif type == 'list':
            assert isinstance(trf, list)
            assert len(trf) == 2
        elif type == 'shapely':
            assert isinstance(trf, Point)
